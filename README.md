# WhatsApp API Sender

### Installing

1.  Clone project from this repo
2.  Install Via NPM `$ cd wa-api-express-js && npm install`    ***or***
3. Install Via YARN ` $cd wa-api-express-js && yarn install`
4. Go to localhost / your port and scan qr code in whatsapp to login

### Feature & Bot Command

 -  Prefix added  **'/'**.
 -  Mismatch prefix protect
 -  Main Whatsapp Logs
 -  API Endpoint
 
#####  Bot Command
  > /author
  > /help
  > /myinfo
  > /api 1
  > /api 2
  
  #####  API Endpoint
`API Send Message` : <https://localhost:{your_port}/api/send>

`API Send Message With Command` : <https://localhost:{your_port}/api/send/command/{your_command}>

####Api Request
                
- Method : POST
- Request Body API Send Message: `number(string), message(string)`
- Request Body API Send Message W Command: `number(string)`

###Depedencys
                    
                    
Package Name  | Role
------------- | -------------
`axios`  | Dependencies
`express`  | Dependencies
`express-fileupload`  | Dependencies
`express-validator`  | Dependencies
`http`  | Dependencies
`mime-types`  | Dependencies
`moment`  | Dependencies
`qrcode`  | Dependencies
`socket.io`  | Dependencies
`whatsapp-web.js`  | Dependencies 
`@types/express`  | Dev Dependencies
`@types/qrcode-terminal`  | Dev Dependencies
`@typescript-eslint/eslint-plugin`  | Dev Dependencies
`@typescript-eslint/parser`  | Dev Dependencies
`eslint`  | Dev Dependencies
`eslint-plugin-simple-import-sort`  | Dev Dependencies
`nodemon`  | Dev Dependencies
`ts-node`  | Dev Dependencies
`typescript`  | Dev Dependencies


_&copy; 2023 Elcode_
