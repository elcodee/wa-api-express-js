const { Client, MessageMedia, LocalAuth } = require('whatsapp-web.js');
const express = require('express');
const { body, validationResult } = require('express-validator');
const socketIO = require('socket.io');
const qrcode = require('qrcode');
const http = require('http');
const fs = require('fs');
const { phoneNumberFormatter } = require('./utils/formatter');
const fileUpload = require('express-fileupload');
const axios = require('axios');
const mime = require('mime-types');
const moment = require('moment');

const port = process.env.PORT || 1500;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

/**
 * NOTE
 * 
 * Banyak orang bingung peringatan buat upload file
 * Jadi, di off in debugnya.
 */
app.use(fileUpload({
  debug: false
}));

app.get('/', (req: any, res: any) => {
  res.sendFile('/views/index.html', {
    root: __dirname
  });
});

const client = new Client({
  restartOnAuthFail: true,
  puppeteer: {
    headless: true,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--no-first-run',
      '--no-zygote',
      '--disable-gpu'
    ],
  },
  authStrategy: new LocalAuth()
});

client.on('message', async (msg: any) => {
  const message = msg.body.toLowerCase();
  let arrStr = message.split("");
  var strRegExp = /\W/g;
  let arrNonWord: any = "";
  let arrCommand: any = message.replace(/[^\w\s]/gi, '')

  arrStr.forEach(function (str: any) {
    var result = str.match(strRegExp);
    if (result)
      arrNonWord = result[0];
  });

  let contactInfo = await msg.getContact();

  if (message === '/author') {
    msg.reply(`Rama | s.id/elcodee`);
  } else if (message === '/help') {
    let replyMsgMenu = `*Helper Commands*\n
/author
/myinfo
/api 1   (_Send Msg_)
/api 2   (_Send Msg With Command_)
/google  (_Soon_)
/chat gpt  (_Soon_)
_*...etc lot of feature coming soon*_\n\n Use *Prefix '/'* For Commands`;

    msg.reply(replyMsgMenu);
  } else if (message === '/myinfo') {
    let myinfo = `*My Info*\n
*Phone Number :* ${contactInfo.number}
*Original Name :* ${contactInfo.pushname}
*Saved Name :* ${contactInfo.name}
*Server :* ${contactInfo.id.server}
*Registered On Whatsapp :* ${contactInfo.isUser ? 'Yes' : 'No'}
*Contact Saved :* ${contactInfo.isMyContact ? 'Yes' : 'No'}
*Whatsapp Bussiness :* ${contactInfo.isBusiness ? 'Yes' : 'No'}
*Blocked :* ${contactInfo.isBlocked ? 'Yes' : 'No'}`;

    msg.reply(myinfo);
  } else if (message === '/api 1') {
    let myinfo = `*API Endpoint Send Message*\n
*URL :* http://localhost:1500/api/send
*Method :* POST
*Request Body :* number(string), message(string)\n\nContact Author For More Details`;

    msg.reply(myinfo);
  } else if (message === '/api 2') {
    let myinfo = `*API Endpoint Send Message With Command*\n
*URL :* http://localhost:1500/api/send/command/{{here}}
*Method :* POST
*Request Body :* number(string)\n\nContact Author For More Details`;

    msg.reply(myinfo);
  } else if(arrNonWord !== "/"){
    msg.reply(`Prefix Not Recognized | Try use *'/${arrCommand}'*`);
  } else {
    msg.reply(`Command Not Found! Type */help* For Command List`);
  }



  //   switch (message) {
  //     case "/author":
  //       msg.reply(`Rama | s.id/elcodee`);
  //     break;
  //     case "/help":
  // let replyMsgMenu = `*Helper Commands*\n
  // ➡ /author
  // ➡ /myinfo
  // ➡ /google  (_Soon_)
  // ➡ /chat gpt  (_Soon_)
  // ➡ _*...etc lot of feature coming soon*_\n\n_WHATSAPP API ENDPOINT CONTACT OWNER FOR DETAIL: wa.me/6281932709954 DIRECTLY_`;

  //       msg.reply(replyMsgMenu);
  //     break;
  //     case "/myinfo":
  // let myinfo = `*My Info*\n
  // Phone Number : ${contactInfo.number}
  // Original Name : ${contactInfo.pushname}
  // Saved Name : ${contactInfo.name}
  // Server : ${contactInfo.id.server}
  // Registered On Whatsapp : ${contactInfo.isUser},
  // Contact Saved : ${contactInfo.isMyContact},
  // Whatsapp Bussiness : ${contactInfo.isBusiness},
  // Contact Saved : ${contactInfo.isMyContact},
  // Blocked : ${contactInfo.isBlocked}`;

  //       msg.reply(myinfo);
  //     break;
  //       default:
  //     break;
  //   }


  if (message === 'grup') {
    client.getChats().then((chats: any) => {
      const groups = chats.filter((chat: any) => chat.isGroup);

      if (groups.length == 0) {
        msg.reply('kamu tidak punya grup.');
      } else {
        let replyMsg = '*GRUP*\n\n';
        groups.forEach((group: any, i: any) => {
          replyMsg += `ID: ${group.id._serialized}\nNama Grup: ${group.name}\n\n`;
        });
        replyMsg += '_Kamu dapat menggunakan id grup untuk mengirim pesan ke grup._'
        msg.reply(replyMsg);
      }
    });
  }
});

client.initialize();

// Socket IO (Realtime Notifications)
io.on('connection', function (socket: any) {
  socket.emit('message', `Menyambungkan... | ${moment().format("dddd MM MMM h:mm:ss A")}`);

  client.on('qr', (qr: any) => {
    qrcode.toDataURL(qr, (err: any, url: any) => {
      socket.emit('qr', url);
      socket.emit('message', `QR Code dibuat, scan di whatsApp! | ${moment().format("dddd MM MMM h:mm:ss A")}`);
    });
  });

  client.on('ready', async () => {
    client.sendMessage(client.info.me._serialized, `_WA API is Ready  |  ${moment().format("MM MMM h:mm:ss A")}_`)
    socket.emit('ready', 'WA API ready!');
    socket.emit('message', `WA API ready! | ${moment().format("dddd-MM-MMM-h:mm:ss A")}`);
  });

  client.on('disconnected', (reason: any) => {
    socket.emit('message', 'WA API Terputus!');
    client.destroy();
    client.initialize();
  });
});


const checkRegisteredNumber = async function (number: any) {
  const isRegistered = await client.isRegisteredUser(number);
  return isRegistered;
}

// Enpoint Send message request
app.post('/api/send', [
  body('number').notEmpty(),
  body('message').notEmpty(),
], async (req: any, res: any) => {
  const errors = validationResult(req).formatWith(({
    msg
  }: any) => {
    return msg;
  });

  if (!errors.isEmpty()) {
    return res.status(422).json({
      status: false,
      message: errors.mapped()
    });
  }

  const number = phoneNumberFormatter(req.body.number);
  const message = req.body.message;

  const isRegisteredNumber = await checkRegisteredNumber(number);

  console.log("NUM : ", number);

  if (!isRegisteredNumber) {
    return res.status(422).json({
      status: false,
      httpCode: 505,
      message: 'Nomor tidak terdaftar di whatsapp'
    });
  }

  client.sendMessage(number, message).then((response: any) => {
    const responseWA: any = `*Response From API*\n
_Status: *Success*_
_To: ${req.body.number}_
_Message: ${response.body}_
_Method: POST_
_Endpoint: {url}/api/send_
_Time: ${moment(response.timestamp).format("ddd MM MMM h:mm:ss A")}_`

    client.sendMessage(client.info.me._serialized, responseWA)

    res.status(200).json({
      status: true,
      httpCode: 200,
      message: "Send Message Success",
      provider: "ELCODE-API-SENDER",
      data: {
        to: response.id.remote.user,
        message: response.body,
        type: response.type,
        timestamp: moment(response.timestamp).format("ddd MM MMM h:mm:ss A"),
      }
    });
  }).catch((err: any) => {
    const responseWA: any = `*Response From API*\n
_Status: *Failed*_
_To: ${req.body.number}_
_Message: ${req.body.message}_
_Method: POST_
_Endpoint: {url}/api/send_
_Time: ${moment().format("ddd MM MMM h:mm:ss A")}_`

    client.sendMessage(client.info.me._serialized, responseWA)

    res.status(500).json({
      status: false,
      httpCode: 500,
      message: "Send Message Failed",
      provider: "ELCODE-API-SENDER",
      data: err
    });
  });
});

// Enpoint Send message request by command (PROGRESS)
app.post('/api/send/command/:command', async (req: any, res: any) => {
  const number = phoneNumberFormatter(req.body.number);

  const isRegisteredNumber = await checkRegisteredNumber(number);

  if (!isRegisteredNumber) {
    return res.status(422).json({
      status: false,
      httpCode: 505,
      message: 'Nomor tidak terdaftar di whatsapp'
    });
  }

  switch (req.params.command) {
    case "menu":
      let replyMsgMenu = `*MENU*\n
➡ author
➡ google search [keywords]  (_Soon_)
➡ chat gpt open ai  (_Soon_)
➡ _*...etc lot of feature coming soon*_`;

      client.sendMessage(number, replyMsgMenu).then((response: any) => {
        const responseWA: any = `*Response From API*\n
      _Status: *Success*_
      _To: ${req.body.number}_
      _Method: POST_
      _Endpoint: {url}/api/send/command_
      _Time: ${moment(response.timestamp).format("ddd MM MMM h:mm:ss A")}_`

        client.sendMessage(client.info.me._serialized, responseWA)

        res.status(200).json({
          status: true,
          httpCode: 200,
          message: "Send Message Command Success",
          provider: "ELCODE-API-SENDER",
          data: {
            to: response.id.remote.user,
            timestamp: moment(response.timestamp).format("ddd MM MMM h:mm:ss A"),
          }
        });
      }).catch((err: any) => {
        const responseWA: any = `*Response From API*\n
      _Status: *Failed*_
      _To: ${req.body.number}_
      _Method: POST_
      _Endpoint: {url}/api/send/command_
      _Time: ${moment().format("ddd MM MMM h:mm:ss A")}_`

        client.sendMessage(client.info.me._serialized, responseWA)

        res.status(500).json({
          status: false,
          httpCode: 500,
          message: "Send Message Command Failed",
          provider: "ELCODE-API-SENDER",
          data: err
        });
      });
      break;
    case "author":
      client.sendMessage(number, `Rama  | s.id/elcodee`).then((response: any) => {
        const responseWA: any = `*Response From API*\n
      _Status: *Success*_
      _To: ${req.body.number}_
      _Method: POST_
      _Endpoint: {url}/api/send/command_
      _Time: ${moment(response.timestamp).format("ddd MM MMM h:mm:ss A")}_`

        client.sendMessage(client.info.me._serialized, responseWA)

        res.status(200).json({
          status: true,
          httpCode: 200,
          message: "Send Message Command Success",
          provider: "ELCODE-API-SENDER",
          data: {
            to: response.id.remote.user,
            timestamp: moment(response.timestamp).format("ddd MM MMM h:mm:ss A"),
          }
        });
      }).catch((err: any) => {
        const responseWA: any = `*Response From API*\n
      _Status: *Failed*_
      _To: ${req.body.number}_
      _Method: POST_
      _Endpoint: {url}/api/send/command_
      _Time: ${moment().format("ddd MM MMM h:mm:ss A")}_`

        client.sendMessage(client.info.me._serialized, responseWA)

        res.status(500).json({
          status: false,
          httpCode: 500,
          message: "Send Message Command Failed",
          provider: "ELCODE-API-SENDER",
          data: err
        });
      });
      break;

    default:
      break;
  }

  //   if(req.params.command === "menu"){
  //     let replyMsgMenu = `*MENU*\n
  // ➡ Help
  // ➡ Google Search Keywords  (_Soon_)
  // ➡ Chat GPT Open AI  (_Soon_)
  // ➡ _*...etc lot of feature coming soon*_`;

  //     client.sendMessage(number, replyMsgMenu).then((response: any) => {
  //   const responseWA: any = `*Response From API*\n
  // _Status: *Success*_
  // _To: ${req.body.number}_
  // _Method: POST_
  // _Endpoint: {url}/api/send/command_
  // _Time: ${moment(response.timestamp).format("ddd MM MMM h:mm:ss A")}_`

  //       client.sendMessage(client.info.me._serialized, responseWA)

  //       res.status(200).json({
  //         status: true,
  //         httpCode: 200,
  //         message: "Send Message Command Success",
  //         provider: "ELCODE-API-SENDER",
  //         data: {
  //           to: response.id.remote.user,
  //           timestamp: moment(response.timestamp).format("ddd MM MMM h:mm:ss A"),
  //         }
  //       });
  //     }).catch((err: any) => {
  //   const responseWA: any = `*Response From API*\n
  // _Status: *Failed*_
  // _To: ${req.body.number}_
  // _Method: POST_
  // _Endpoint: {url}/api/send/command_
  // _Time: ${moment().format("ddd MM MMM h:mm:ss A")}_`

  //       client.sendMessage(client.info.me._serialized, responseWA)

  //       res.status(500).json({
  //         status: false,
  //         httpCode: 500,
  //         message: "Send Message Command Failed",
  //         provider: "ELCODE-API-SENDER",
  //         data: err
  //       });
  //     });
  //   }

});

server.listen(port, function () {
  console.log('App running on port: ' + port);
});
